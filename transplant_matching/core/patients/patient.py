from typing import Dict, List

from transplant_matching.utils.patient_naming import get_country

NAME = "name"
BLOOD_GROUP = "blood_group"
HLA_ANTIGENS = "hla_antigens"
HLA_ANTIBODIES = "hla_antibodies"
ACCEPTABLE_BLOOD_GROUPS = "acceptable_blood_groups"
DONOR_NAME = "donor_name"

ID = "_id"
TYPE = "_type"
PAYLOAD = "_payload"

RECIPIENT_PATIENT_TYPE = "Recipient"
DONOR_PATIENT_TYPE = "Donor"

ANTIGENS = "antigens"
ANTIBODIES = "antibodies"


class Patient(object):
    def __init__(self, name: str):
        self._name = name

    def __str__(self):
        return self._name

    @property
    def name(self) -> str:
        return self._name

    def _get_payload(self) -> Dict:
        raise NotImplementedError("Has to be overriden.")

    def serialize_to_dict(self) -> Dict:
        dictionary = {ID: self.name,
                      TYPE: type(self).__name__,
                      PAYLOAD: self._get_payload()}
        return dictionary

    def is_recipient(self) -> bool:
        return self.get_type() == RECIPIENT_PATIENT_TYPE

    def is_donor(self) -> bool:
        return self.get_type() == DONOR_PATIENT_TYPE

    def get_hla_of_groups(self, groups: List[str], type: str = ANTIGENS, complement: bool = False) -> List[str]:
        if type == ANTIGENS:
            all_elements = self.hla_antigens
        elif type == ANTIBODIES:
            all_elements = self.hla_antibodies
        else:
            raise ValueError(f"Type argument can have only values {ANTIBODIES} / {ANTIGENS}")
        condition = lambda element: any([element.startswith(g) for g in groups])
        selected_elements = [element for element in all_elements if condition(element)]
        if complement:
            selected_elements = [element for element in all_elements if element not in selected_elements]
        if len(groups) == 1:
            group = groups[0]
            selected_elements.sort(key=lambda string: (int)(string[len(group):]))
        return selected_elements

    def get_antigens_of_group(self, group: str) -> List[str]:
        return self.get_hla_of_groups([group], ANTIGENS)

    def get_antibodies_of_group(self, group: str) -> List[str]:
        return self.get_hla_of_groups([group], ANTIBODIES)

    def get_complementary_antigens(self, groups: List[str]) -> List[str]:
        complement = True
        return self.get_hla_of_groups(groups, ANTIGENS, complement)

    def get_complementary_antibodies(self, groups: List[str]) -> List[str]:
        complement = True
        return self.get_hla_of_groups(groups, ANTIBODIES, complement)

    @classmethod
    def get_type(cls) -> str:
        return cls.__name__

    def get_country(self) -> str:
        return get_country(self.name)
