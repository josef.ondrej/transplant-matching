from typing import List, Dict

from transplant_matching.core.patients.patient import Patient, BLOOD_GROUP, HLA_ANTIGENS, HLA_ANTIBODIES, \
    ACCEPTABLE_BLOOD_GROUPS, NAME, DONOR_NAME


class Recipient(Patient):
    def __init__(self, name: str, blood_group: str = None, hla_antigens: List[str] = None,
                 hla_antibodies: Dict[str, float] = None, acceptable_blood_groups: List[str] = None,
                 donor_name: str = None):
        super().__init__(name)
        self._blood_group = blood_group
        self._hla_antigens = hla_antigens or []
        self._hla_antibodies = hla_antibodies or dict()
        self._acceptable_blood_groups = acceptable_blood_groups or []
        self._donor_name = donor_name

    @property
    def blood_group(self) -> str:
        return self._blood_group

    @property
    def hla_antigens(self) -> List[str]:
        return self._hla_antigens

    @property
    def hla_antibodies(self) -> Dict[str, float]:
        return self._hla_antibodies


    @property
    def acceptable_blood_groups(self) -> List[str]:
        return self._acceptable_blood_groups

    @property
    def donor_name(self) -> str:
        return self._donor_name

    def _get_payload(self) -> Dict:
        payload = dict()
        payload[NAME] = self.name
        payload[BLOOD_GROUP] = self.blood_group
        payload[HLA_ANTIGENS] = self.hla_antigens
        payload[HLA_ANTIBODIES] = self.hla_antibodies
        payload[ACCEPTABLE_BLOOD_GROUPS] = self.acceptable_blood_groups
        payload[DONOR_NAME] = self.donor_name
        return payload
