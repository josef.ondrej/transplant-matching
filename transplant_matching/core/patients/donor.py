from typing import List, Dict

from transplant_matching.core.patients.patient import Patient, NAME, BLOOD_GROUP, HLA_ANTIGENS


class Donor(Patient):
    def __init__(self, name: str, blood_group: str = None, hla_antigens: List[str] = None):
        super().__init__(name)
        self._blood_group = blood_group
        self._hla_antigens = hla_antigens

    @property
    def blood_group(self) -> str:
        return self._blood_group

    @property
    def hla_antigens(self) -> List[str]:
        return self._hla_antigens

    def _get_payload(self) -> Dict:
        payload = dict()
        payload[NAME] = self.name
        payload[BLOOD_GROUP] = self.blood_group
        payload[HLA_ANTIGENS] = self.hla_antigens
        return payload
