from typing import List, Tuple, Set

from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.patient import Patient
from transplant_matching.core.patients.recipient import Recipient
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase
from transplant_matching.utils.patient_naming import get_country, get_pair_name

SERIALIZATION_TO_GET_SEPARATOR_PATIENT = ","
SERIALIZATION_TO_GET_SEPARATOR_PATIENT_TYPE = ";"


class Chain(object):
    def __init__(self, donors: List[Donor], recipients: List[Recipient]):
        assert len(donors) == len(recipients)
        self._donors = donors
        self._recipients = recipients
        self._score = None

    def __str__(self):
        pairs = [f"{str(donor)} > {str(recipient)}" for donor, recipient in zip(self.donors, self.recipients)]
        return " | ".join(pairs)

    @property
    def donors(self) -> List[Donor]:
        return self._donors

    @property
    def recipients(self) -> List[Recipient]:
        return self._recipients

    @property
    def pairs(self) -> List[Tuple[Donor, Recipient]]:
        return list(zip(self.donors, self.recipients))

    @property
    def length(self) -> int:
        assert len(self.donors) == len(self.recipients)
        return len(self.donors)

    @property
    def is_cycle(self) -> bool:
        donor_pair_names = {get_pair_name(donor.name) for donor in self.donors}
        recipient_pair_names = {get_pair_name(recipient.name) for recipient in self.recipients}
        return donor_pair_names == recipient_pair_names

    def get_score(self, scorer: TransplantScorerBase) -> float:
        total_score = 0
        for donor, recipient in zip(self.donors, self.recipients):
            transplant_score = scorer.score_transplant(donor, recipient)
            total_score += transplant_score

        return total_score

    def serialize_to_GET(self) -> str:
        serialized_donors = SERIALIZATION_TO_GET_SEPARATOR_PATIENT.join([donor.name for donor in self.donors])
        serialized_recipients = SERIALIZATION_TO_GET_SEPARATOR_PATIENT.join(
            [recipient.name for recipient in self.recipients])
        return serialized_donors + SERIALIZATION_TO_GET_SEPARATOR_PATIENT_TYPE + serialized_recipients

    @staticmethod
    def deserialize_from_GET(serialized_chain: str, patients: List[Patient]) -> "Chain":
        patient_name_to_patient = {patient.name: patient for patient in patients}
        serialized_donors, serialized_recipients = serialized_chain.split(SERIALIZATION_TO_GET_SEPARATOR_PATIENT_TYPE)
        donor_names = serialized_donors.split(SERIALIZATION_TO_GET_SEPARATOR_PATIENT)
        recipient_names = serialized_recipients.split(SERIALIZATION_TO_GET_SEPARATOR_PATIENT)
        donors = [patient_name_to_patient[donor_name] for donor_name in donor_names]
        recipients = [patient_name_to_patient[recipient_name] for recipient_name in recipient_names]
        return Chain(donors, recipients)

    def contains(self, name: str) -> bool:
        for recipient in self.recipients:
            if recipient.name == name:
                return True

        for donor in self.donors:
            if donor.name == name:
                return True

        return False

    def get_countries(self) -> Set[str]:
        donor_countries = {get_country(donor.name) for donor in self.donors}
        recipient_countries = {get_country(recipient.name) for recipient in self.recipients}
        return set.union(donor_countries, recipient_countries)

    def get_number_of_transplants(self) -> int:
        n_transplants = len(self._donors)
        if self.is_cycle:
            n_transplants -= 1

        return n_transplants

    def get_min_transplant_score(self, scorer: TransplantScorerBase) -> float:
        min_transplant_score = float("inf")
        for donor, recipient in zip(self.donors, self.recipients):
            min_transplant_score = min(min_transplant_score, scorer.score_transplant(donor, recipient))
        return min_transplant_score
