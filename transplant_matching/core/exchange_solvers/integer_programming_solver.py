import itertools
from typing import List, Tuple, Iterator, Dict

import numpy as np
from mip import Model, BINARY, xsum, maximize

from transplant_matching.core.exchange_solvers.exchange_solver_base import ExchangeSolverBase
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase
from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient


class IntegerProgrammingSolver(ExchangeSolverBase):
    def __init__(self, donors: List[Donor], recipients: List[Recipient], scorer: TransplantScorerBase,
                 max_chain_length: int = None):
        super().__init__(donors, recipients, scorer)
        self._max_chain_length = max_chain_length

    def _solve(self, score_matrix: np.ndarray) -> Iterator[List[Tuple[int, int]]]:
        model = Model("pairing")

        potential_pairs = self._get_potential_pairs(score_matrix)
        original_pairs = self._get_original_pairs(score_matrix)

        donor_to_potential_recipients = self._get_donor_to_potential_recipients_dict(potential_pairs)
        recipient_to_potential_donors = self._get_recipient_to_potential_donors_dict(potential_pairs)

        pair_included_indicator = {(donor, recipient): model.add_var(var_type=BINARY)
                                   for donor, recipient in potential_pairs}

        model.objective = maximize(
            xsum([score_matrix[donor, recipient] * pair_included_indicator[(donor, recipient)]
                  for donor, recipient in potential_pairs]))

        # Each donor gives at most one kidney
        for donor in donor_to_potential_recipients.keys():
            model += xsum([pair_included_indicator[(donor, recipient)]
                           for recipient in donor_to_potential_recipients[donor]]) <= 1

        # Each recipient gets at most one kidney
        for recipient in recipient_to_potential_donors.keys():
            model += xsum([pair_included_indicator[(donor, recipient)]
                           for donor in recipient_to_potential_donors[recipient]]) <= 1

        # Recipient has to receive more (or the same amount) of kidneys that his donor gave
        for donor, recipient in original_pairs:
            pair_received_kidney_count = xsum([pair_included_indicator[(k, recipient)]
                                               for k in recipient_to_potential_donors.get(recipient, [])])

            pair_given_kidney_count = xsum([pair_included_indicator[(donor, k)]
                                            for k in donor_to_potential_recipients.get(donor, [])])

            model += (pair_received_kidney_count - pair_given_kidney_count) >= 0

        if self._max_chain_length is not None:
            recipient_to_original_donor = {recipient: donor for donor, recipient in original_pairs}
            paired_donors = [donor for donor, recipient in original_pairs]

            paths_without_altruists = self._generate_paths_of_length(self._max_chain_length, paired_donors,
                                                                     donor_to_potential_recipients,
                                                                     recipient_to_original_donor)
            for path in paths_without_altruists:
                active_exchanges_in_path_count = xsum([pair_included_indicator[(donor, recipient)]
                                                       for donor, recipient in path])
                model += active_exchanges_in_path_count <= (self._max_chain_length - 1)

        model.optimize()

        assignment = [edge for edge, value in pair_included_indicator.items() if value.x]

        yield assignment

    def _generate_paths_of_length(self, lenght: int, starting_donors: List[int],
                                  potential_recipients: Dict[int, List[int]],
                                  recipient_to_original_donor: Dict[int, int]) -> List[List[Tuple[int, int]]]:
        if lenght == 0:
            return [[]]
        paths = []
        for donor in starting_donors:
            for recipient in potential_recipients.get(donor, []):
                starting_pair = (donor, recipient)
                next_donor = recipient_to_original_donor[recipient]
                new_potential_recipients = {d: [r for r in recipients if r != recipient]
                                            for d, recipients in potential_recipients.items()
                                            if d != donor}
                continuing_paths = self._generate_paths_of_length(lenght - 1, [next_donor], new_potential_recipients,
                                                                  recipient_to_original_donor)
                paths.extend([[starting_pair] + path for path in continuing_paths])
        return paths


    def _get_all_pairs(self, score_matrix: np.ndarray) -> List[Tuple[int, int]]:
        donor_count, recipient_count = score_matrix.shape
        all_pairs = list(itertools.product(range(donor_count), range(recipient_count)))
        return all_pairs

    def _get_potential_pairs(self, score_matrix: np.ndarray) -> List[Tuple[int, int]]:
        all_pairs = self._get_all_pairs(score_matrix)
        potential_pairs = [(donor, recipient) for (donor, recipient) in all_pairs
                           if self._is_number(score_matrix[donor, recipient])]
        return potential_pairs

    def _get_original_pairs(self, score_matrix: np.ndarray) -> List[Tuple[int, int]]:
        all_pairs = self._get_all_pairs(score_matrix)
        original_pairs = [(donor, recipient) for (donor, recipient) in all_pairs
                          if np.isnan(score_matrix[donor, recipient])]
        return original_pairs

    def _get_single_donors(self, score_matrix: np.ndarray) -> List[int]:
        donor_count, recipient_count = score_matrix.shape
        single_donors = [donor for donor in range(donor_count) if all(~np.isnan(score_matrix[donor, :]))]
        return single_donors

    def _get_donor_to_potential_recipients_dict(self, potential_pairs: List[Tuple[int, int]]) -> Dict[int, List[int]]:
        donors = list(set([pair[0] for pair in potential_pairs]))
        recipients = list(set([pair[1] for pair in potential_pairs]))
        donor_to_potential_recipients = {
            donor: [recipient for recipient in recipients if (donor, recipient) in potential_pairs]
            for donor in donors}
        return donor_to_potential_recipients

    def _get_recipient_to_potential_donors_dict(self, potential_pairs: List[Tuple[int, int]]) -> Dict[int, List[int]]:
        donors = list(set([pair[0] for pair in potential_pairs]))
        recipients = list(set([pair[1] for pair in potential_pairs]))
        donor_to_potential_recipients = {
            recipient: [donor for donor in donors if (donor, recipient) in potential_pairs]
            for recipient in recipients}
        return donor_to_potential_recipients

    def _is_number(self, score_matrix_entry) -> bool:
        is_number = not np.isnan(score_matrix_entry) and score_matrix_entry != np.NINF
        return is_number
