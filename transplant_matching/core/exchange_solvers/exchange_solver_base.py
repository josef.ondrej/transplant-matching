from typing import List, Tuple, Iterator

import matplotlib.pyplot as plt
import numpy as np

from transplant_matching.core.configs.solver_config import SolverConfig
from transplant_matching.core.exchange_solvers.chain import Chain
from transplant_matching.core.exchange_solvers.exchange import Exchange
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase
from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient
from transplant_matching.utils.patient_naming import are_pair, get_pair_name


class ExchangeSolverBase(object):
    def __init__(self, donors: List[Donor], recipients: List[Recipient], scorer: TransplantScorerBase,
                 config: SolverConfig = None):
        self._donors = donors
        self._recipients = recipients
        self._scorer = scorer
        self._config = config

    @property
    def donors(self) -> List[Donor]:
        return self._donors

    @property
    def recipients(self) -> List[Recipient]:
        return self._recipients

    @property
    def scorer(self) -> TransplantScorerBase:
        return self._scorer

    @property
    def config(self) -> SolverConfig:
        return self._config

    def get_exchange_iterator(self) -> Iterator[Exchange]:
        if len(self._donors) == 0 or len(self._recipients) == 0:
            return []

        score_matrix = self._create_score_matrix()
        solution_iterator = self._solve(score_matrix)
        for solution in solution_iterator:
            exchange = self._get_exchange_from_solution(solution)
            yield exchange

    def _solve(self, score_matrix: np.ndarray) -> Iterator[List[Tuple[int, int]]]:
        """
        Returns iterator over the optimal matching. The result is a list of pairs. Each pair consists of two integers
        which correspond to recipient, donor indices in the self._donors, resp. self._recipients lists.

        :param score_matrix: matrix of Score(i,j) for transplant from donor_i to recipient_j
            special values are:
            UNACCEPTABLE_SCORE = np.NINF
            DEFAULT_DONOR_RECIPIENT_PAIR_SCORE = np.NAN
        """
        raise NotImplementedError("Has to be overriden.")

    def _create_score_matrix(self) -> np.ndarray:
        score_matrix = np.zeros((len(self._donors), len(self._recipients)))

        donor_name_to_donor = {donor.name: donor for donor in self._donors}

        for donor_index, donor in enumerate(self._donors):
            for recipient_index, recipient in enumerate(self._recipients):
                original_donor = donor_name_to_donor[recipient.donor_name]
                transplant_score = self._scorer.score_transplant_considering_original_donor(donor, original_donor,
                                                                                            recipient)
                score_matrix[donor_index, recipient_index] = transplant_score

        return score_matrix

    def _get_exchange_from_solution(self, solution: List[Tuple[int, int]]) -> Exchange:
        links = [(self._donors[link[0]].name, self._recipients[link[1]].name) for link in solution]
        links = [tuple(get_pair_name(link_part) for link_part in link) for link in links]
        pair_name_chains = self._build_chains_from_links(links)

        pair_name_to_donor = {get_pair_name(donor.name): donor for donor in self._donors}
        pair_name_to_recipient = {get_pair_name(recipient.name): recipient for recipient in self._recipients}
        patient_chains = [[(pair_name_to_donor[link[0]], pair_name_to_recipient[link[1]]) for link in chain]
                          for chain in pair_name_chains]
        patient_chains = [[link for link in chain if not are_pair(link[0].name, link[1].name)] for chain in
                          patient_chains]
        patient_chains = [chain for chain in patient_chains if len(chain) > 0]
        chains = [Chain(*list(zip(*chain))) for chain in patient_chains]
        exchange = Exchange(chains)
        return exchange

    def _build_chain_from_links(self, links: List[Tuple[str, str]]) -> List[Tuple[str, str]]:
        next_link = {link[0]: link for link in links}
        previous_link = {link[1]: link for link in links}

        chain = []

        start_link = links[0]
        used_links = set()

        link = start_link
        while link is not None and link not in used_links:
            chain.append(link)
            used_links.add(link)
            link = next_link.get(link[1])

        link = previous_link.get(start_link[0])
        while link is not None and link not in used_links:
            chain = [link] + chain
            used_links.add(link)
            link = previous_link.get(link[0])

        return chain

    def _build_chains_from_links(self, links: List[Tuple[str, str]]) -> List[List[Tuple[str, str]]]:

        """
        Return list of longest possible chains from a list of links, each link is represented by (source, target) string tuple.
        Chain is a sequence of connected links which can be either circular or linear
        e.g. [(A,B), (B,C), (C,A)] or [(A,B), (B,C), (C,D), (D, E)]
        """
        chains = []
        while len(links) > 0:
            chain = self._build_chain_from_links(links)
            chains.append(chain)
            for assignment in chain:
                links.remove(assignment)

        return chains

    def _plot_score_matrix(self):
        pair_name_to_index = {get_pair_name(donor.name): str(ix) for ix, donor in enumerate(self._donors)}

        donor_names = [get_pair_name(donor.name) for donor in self._donors]
        recipent_names = [get_pair_name(recipient.name) for recipient in self._recipients]

        donor_names = [pair_name_to_index[dn] + ": " + dn for dn in donor_names]
        recipent_names = [pair_name_to_index[rn] + ": " + rn for rn in recipent_names]

        score_matrix = self._create_score_matrix()
        plt.figure(figsize=(10, 10))
        plt.imshow(np.isfinite(score_matrix), cmap="gray_r")
        plt.imshow(np.isnan(score_matrix), cmap="Reds", alpha=0.5)
        plt.xticks(range(len(recipent_names)), recipent_names, rotation=90)
        plt.yticks(range(len(donor_names)), donor_names)
        plt.grid()
        plt.show()

    def _get_bridge_indices(self, score_matrix: np.ndarray) -> List[int]:
        bridge_indices = np.where(np.sum(np.isnan(score_matrix), axis=1) == 0)[0]
        return list(bridge_indices)
