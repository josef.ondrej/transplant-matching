from typing import Iterator, List, Tuple, Dict, Set

import matplotlib.pyplot as plt
import numpy as np
from graph_tool import topology
from graph_tool.all import *

from transplant_matching.core.configs.solver_configs.all_solutions_solver_config import AllSolutionsSolverConfig
from transplant_matching.core.exchange_solvers.exchange_solver_base import ExchangeSolverBase
from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase
from transplant_matching.utils.patient_naming import get_pair_name


class AllSolutionsSolver(ExchangeSolverBase):
    """
    Finds all maximal exchanges -- that is all possible exchanges are either included in result of this solver
    or are subset of some exchange of this solver.
    """

    def __init__(self, donors: List[Donor], recipients: List[Recipient], scorer: TransplantScorerBase,
                 config: AllSolutionsSolverConfig, verbose: bool = False):
        super().__init__(donors, recipients, scorer, config)
        self._verbose = verbose

    def _solve(self, score_matrix: np.ndarray) -> Iterator[List[Tuple[int, int]]]:
        """
        Returns iterator over the optimal matching. The result is a list of pairs. Each pair consists of two integers
        which correspond to recipient, donor indices in the self._donors, resp. self._recipients lists.

        :param score_matrix: matrix of Score(i,j) for transplant from donor_i to recipient_j
            special values are:
            UNACCEPTABLE_SCORE = np.NINF
            DEFAULT_DONOR_RECIPIENT_PAIR_SCORE = np.NAN
        """
        graph, _ = self._graph_from_score_matrix(score_matrix)

        pure_circuits = [tuple(circuit) for circuit in self._find_all_circuits(graph)]
        if self.config.max_transplants_in_chain:
            pure_circuits = [circuit for circuit in pure_circuits if
                             len(circuit) <= self.config.max_transplants_in_chain]

        bridge_paths = self._find_all_bridge_paths(score_matrix)
        if self.config.max_transplants_in_chain:
            bridge_paths = [path for path in bridge_paths if len(path) <= self.config.max_transplants_in_chain + 1]

        all_paths = pure_circuits + bridge_paths

        if self._verbose:
            print(f"[INFO] Constructing intersection graph, "
                  f"#circuits: {len(pure_circuits)}, #paths: {len(bridge_paths)}")
        intersection_graph, vertex_to_set = self._construct_intersection_graph(all_paths)

        if self._verbose:
            print("[INFO] Listing all max cliques")
        max_cliques = list(topology.max_cliques(intersection_graph))

        if self._verbose:
            print("[INFO] Finding 1 vertex cliques")
        used_vertices = set()
        all_vertices = set([intersection_graph.vertex_index[vertex] for vertex in vertex_to_set.keys()])

        for clique in max_cliques:
            used_vertices.update(clique)

        single_vertex_cliques = [[v] for v in all_vertices - used_vertices]
        max_cliques.extend(single_vertex_cliques)

        if self._verbose:
            print("[INFO] Creating pairings from paths and circuits ")
        pair_index_to_recipient_index = self._construct_pair_index_to_recipient_index(score_matrix)

        for clique in max_cliques:
            circuit_list = [vertex_to_set[v] for v in clique]
            pairs = [[(circuit[i], pair_index_to_recipient_index[circuit[i + 1]])
                      for i in range(len(circuit) - 1)]
                     for circuit in circuit_list]
            pairs = [pair for pair_sublist in pairs for pair in pair_sublist]
            yield pairs

    def _construct_intersection_graph(self, sets: List[Tuple[int]]) -> Tuple[Graph, Dict[int, Tuple[int]]]:
        graph = Graph(directed=False)

        i_to_set = {index: st for index, st in enumerate(sets)}

        unique_indices = set()
        for st in sets:
            unique_indices.update(st)

        index_to_sets_not_having_index = {i: {st for st in sets if i not in st} for i in unique_indices}

        set_to_index = {st: index for index, st in enumerate(sets)}
        n_sets = len(sets)
        adjacency_matrix = np.zeros((n_sets, n_sets))

        for set_1 in sets:
            item_complementary_set_list = [index_to_sets_not_having_index[item] for item in set_1]
            complementary_sets = set.intersection(*item_complementary_set_list)

            index_1 = set_to_index[set_1]
            for set_2 in complementary_sets:
                index_2 = set_to_index[set_2]
                adjacency_matrix[index_1, index_2] = True

        graph.add_edge_list(np.transpose(adjacency_matrix.nonzero()))
        return graph, i_to_set

    def _find_all_circuits(self, graph: Graph) -> Iterator[List[int]]:
        """
        Circuits between pairs, each pair is denoted by it's pair = donor index
        """
        for circuit in topology.all_circuits(graph):
            yield list(circuit)

    def _find_acceptable_recipient_indices(self, score_matrix: np.ndarray, donor_index: int) -> List[int]:
        return list(np.where(np.isfinite(score_matrix[donor_index]))[0])

    def _graph_from_score_matrix(self, score_matrix: np.array,
                                 add_fake_edges_for_bridge_donors: bool = False) -> Tuple[Graph, Dict]:
        n_donors, n_recipients = score_matrix.shape

        recipient_index_to_pair_index = {recipient_ix: donor_ix for donor_ix, recipient_ix in
                                         zip(*np.where(np.isnan(score_matrix)))}

        directed_graph = Graph(directed=True)
        pair_index_to_vertex = {i: directed_graph.add_vertex() for i in range(n_donors)}

        # Add donor -> recipient edges
        for pair_index in range(n_donors):
            source_vertex = pair_index_to_vertex[pair_index]
            recipient_indices = self._find_acceptable_recipient_indices(score_matrix, pair_index)

            if len(recipient_indices) == 0:
                continue

            for recipient_index in recipient_indices:
                target_pair_index = recipient_index_to_pair_index[recipient_index]
                target_vertex = pair_index_to_vertex[target_pair_index]

                directed_graph.add_edge(source_vertex, target_vertex)

        # Add recipient -> bridge donors edges
        if add_fake_edges_for_bridge_donors:
            bridge_indices = self._get_bridge_indices(score_matrix)
            regular_indices = [i for i in range(n_donors) if i not in bridge_indices]
            for bridge_index in bridge_indices:
                bridge_vertex = pair_index_to_vertex[bridge_index]
                for regular_index in regular_indices:
                    regular_vertex = pair_index_to_vertex[regular_index]
                    directed_graph.add_edge(regular_vertex, bridge_vertex)

        self._add_names_to_vertices(directed_graph, pair_index_to_vertex)

        return directed_graph, pair_index_to_vertex

    def _graph_from_adjacency_matrix(self, adjacency_matrix: np.array) -> Tuple[Graph, Dict]:
        """
        Constructs graph_tools.Graph from adjacency matrix
        :param adjacency_matrix: matrix of dtype bool
        :return: directed graph and mapping from indices of adjacency matrix to the vertices of the graph
        """
        directed_graph = Graph(directed=True)
        n_rows, n_cols = adjacency_matrix.shape
        assert n_rows == n_cols

        index_to_vertex = {i: directed_graph.add_vertex() for i in range(n_rows)}

        for donor_index in range(n_rows):
            donor_vertex = index_to_vertex[donor_index]
            recipient_indices = list(np.where(adjacency_matrix[donor_index, :])[0])
            for recipient_index in recipient_indices:
                recipient_vertex = index_to_vertex[recipient_index]
                directed_graph.add_edge(donor_vertex, recipient_vertex)

        self._add_names_to_vertices(directed_graph, index_to_vertex)

        return directed_graph, index_to_vertex

    def _add_names_to_vertices(self, graph: Graph, index_to_vertex: Dict[str, Vertex]):
        patient_pair_name = graph.new_vertex_property("string")
        graph.vertex_properties["patient_pair_name"] = patient_pair_name

        for donor_index, donor in enumerate(self._donors):
            vertex = index_to_vertex[donor_index]
            patient_pair_name[vertex] = get_pair_name(donor.name)

    def _construct_pair_index_to_recipient_index(self, score_matrix: np.ndarray) -> Dict[int, int]:
        pair_index_to_recipient_index = dict()
        n_donor, n_recipient = score_matrix.shape
        for pair_index in range(n_donor):
            recipient_indices = np.where(np.isnan(score_matrix[pair_index, :]))[0]
            if len(recipient_indices) > 0:
                pair_index_to_recipient_index[pair_index] = recipient_indices[0]

        return pair_index_to_recipient_index

    def _plot_adjacency_matrix(self):
        score_matrix = self._create_score_matrix()
        adj_matrix = self._transform_to_directed_graph_adj_matrix(score_matrix)
        donor_names = [donor.name for donor in self._donors]
        plt.figure(figsize=(10, 10))
        plt.imshow(adj_matrix, cmap="gray_r")
        plt.xticks(range(len(donor_names)), donor_names, rotation=90)
        plt.yticks(range(len(donor_names)), donor_names)
        plt.show()

    def _export_graph(self, file_path: str):
        score_matrix = self._create_score_matrix()
        graph, index_to_vertex = self._graph_from_score_matrix(score_matrix)
        graph_draw(graph,
                   vertex_text=graph.vertex_properties["patient_pair_name"],
                   vertex_font_size=12,
                   vertex_size=30,
                   output_size=(1600, 1600),
                   vertex_color="black",
                   edge_marker_size=10,
                   vertex_fill_color="white",
                   output=file_path,
                   edge_pen_width=1.5)

    def _get_donor_pair_index_to_recipient_pairs_indices(self, score_matrix: np.ndarray) -> Dict[int, List[int]]:
        n_donors, _ = score_matrix.shape

        donor_index_to_recipient_indices = {
            donor_index: self._find_acceptable_recipient_indices(score_matrix, donor_index)
            for donor_index in range(n_donors)}

        pair_index_to_recipient_index = self._construct_pair_index_to_recipient_index(score_matrix)
        recipient_index_to_pair_index = {recipient_index: pair_index for pair_index, recipient_index
                                         in pair_index_to_recipient_index.items()}

        donor_pair_index_to_recipient_pair_indices = {
            donor_index: [recipient_index_to_pair_index[recipient_index] for recipient_index in recipient_indices]
            for donor_index, recipient_indices in donor_index_to_recipient_indices.items()}

        return donor_pair_index_to_recipient_pair_indices

    def _find_all_bridge_paths(self, score_matrix: np.ndarray) -> List[Tuple[int]]:
        bridge_indices = self._get_bridge_indices(score_matrix)
        donor_pair_index_to_recipient_pair_indices = self._get_donor_pair_index_to_recipient_pairs_indices(score_matrix)

        paths = []

        for bridge_index in bridge_indices:
            bridge_paths = self._find_all_paths_starting_with(bridge_index, donor_pair_index_to_recipient_pair_indices,
                                                              set(bridge_indices))
            paths.extend(bridge_paths)

        paths = [tuple(path) for path in paths if len(path) > 1]
        return paths

    def _find_all_paths_starting_with(self, source: int, source_to_targets: Dict[int, int], covered_indices: Set) -> \
            List[List[int]]:
        targets = source_to_targets[source]
        remaining_targets = set(targets) - covered_indices

        paths = []
        paths.append([source])

        for target in remaining_targets:
            covered_indices.add(target)
            paths_starting_with_target = self._find_all_paths_starting_with(target, source_to_targets, covered_indices)
            covered_indices.remove(target)

            for path in paths_starting_with_target:
                path.insert(0, source)
            paths.extend(paths_starting_with_target)

        return paths


if __name__ == "__main__":
    x = np.array([np.NAN, np.NINF, 0.0, 9.8])
    indices = list(np.where(np.isfinite(x))[0])
    print(indices)

    matrix = np.array([[1, 2, 3], [4, 5, 6]])
    new_matrix_1 = matrix[-1, :]
    print(new_matrix_1)

    print(matrix.shape)

    score_matrix = np.array([[np.NAN, np.NINF, 10.2, 13.1],
                             [0.2, np.NAN, np.NINF, 1],
                             [0.1, 10.2, 10.3, np.NAN],
                             [np.NINF, np.NINF, np.NAN, 10],
                             [0.2, 0.4, np.NINF, 0.5],
                             [0.2, np.NINF, np.NINF, 0.5]])
