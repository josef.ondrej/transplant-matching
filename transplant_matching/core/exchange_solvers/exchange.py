from typing import List, Set

from transplant_matching.core.exchange_solvers.chain import Chain
from transplant_matching.core.patients.patient import Patient
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase

SERIALIZATION_TO_GET_SEPARATOR = "|"


class Exchange(object):
    def __init__(self, chains: List[Chain]):
        self._chains = chains
        self._score = None

    def __str__(self):
        representation = "\n".join([str(chain) for chain in self.chains])
        return representation

    @property
    def chains(self) -> List[Chain]:
        return self._chains

    def get_score(self, scorer: TransplantScorerBase) -> float:
        total_score = 0
        for chain in self.chains:
            chain_score = chain.get_score(scorer)
            total_score += chain_score

        return total_score

    def serialize_to_GET(self) -> str:
        return SERIALIZATION_TO_GET_SEPARATOR.join([chain.serialize_to_GET() for chain in self.chains])

    @staticmethod
    def deserialize_from_GET(serialized_exchange: str, patients: List[Patient]) -> "Exchange":
        serialized_chains = serialized_exchange.split(SERIALIZATION_TO_GET_SEPARATOR)
        chains = [Chain.deserialize_from_GET(serialized_chain, patients) for serialized_chain in serialized_chains]
        return Exchange(chains)

    def contains(self, name: str) -> bool:
        for chain in self.chains:
            if chain.contains(name):
                return True

        return False

    def get_countries(self) -> Set[str]:
        return set.union(*self.get_chain_countries())

    def get_chain_countries(self) -> List[Set[str]]:
        return [chain.get_countries() for chain in self.chains]

    def get_max_distinct_countries_in_chain_count(self) -> int:
        return max([len(countries) for countries in self.get_chain_countries()])

    def get_max_number_of_transplants(self) -> int:
        return max([chain.get_number_of_transplants() for chain in self.chains])

    def get_min_transplant_score(self, scorer: TransplantScorerBase) -> float:
        return min([chain.get_min_transplant_score(scorer) for chain in self.chains])
