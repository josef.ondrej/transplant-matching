import json
import os
from typing import Iterator, List

from transplant_matching.core.configs.solver_configs.all_solutions_solver_config import AllSolutionsSolverConfig
from transplant_matching.core.configs.solver_configs.cached_solver_config import CachedSolverConfig
from transplant_matching.core.exchange_solvers.all_solutions_solver import AllSolutionsSolver
from transplant_matching.core.exchange_solvers.exchange import Exchange
from transplant_matching.core.exchange_solvers.exchange_solver_base import ExchangeSolverBase
from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase

solver_constructor_name_to_solver_constructor = {"AllSolutionsSolver": AllSolutionsSolver}
solver_constructor_name_to_solver_config_constructor = {"AllSolutionsSolver": AllSolutionsSolverConfig}


class CachedSolver(ExchangeSolverBase):
    def __init__(self, donors: List[Donor], recipients: List[Recipient], scorer: TransplantScorerBase,
                 config: CachedSolverConfig):
        super().__init__(donors, recipients, scorer, config)
        self._solver = self._construct_solver()

    def get_exchange_iterator(self) -> Iterator[Exchange]:
        if not self.config.cache_valid or not os.path.exists(self.config.cache_path):
            print("[INFO] Calculating exchanges from scratch")
            exchanges = list(self._solver.get_exchange_iterator())
            self._save_to_cache(exchanges)
            self.config.validate_cache()

        print("[INFO] Loading exchanges from cache")
        exchanges = self._load_from_cache()

        print("[INFO] Filtering by required names")
        self._filter_required_names(exchanges)

        print("[INFO] Filtering by allowed countries")
        self._filter_allowed_countries(exchanges)

        print("[INFO] Filtering by max number of distinct countries in chain")
        self._filter_max_distinct_countries_in_chain(exchanges)

        print("[INFO] Filtering by max transplants in chain")
        self._filter_max_number_of_transplants_in_chain(exchanges)

        print("[INFO] Filtering by min transplant score")
        self._filter_min_transplant_score(exchanges)

        print(f"[INFO] Sorting exchanges by {self._solver._scorer.__class__.__name__} scorer")
        exchanges.sort(key=lambda exchange: exchange.get_score(self._solver._scorer), reverse=True)

        if self.config.max_exchanges is not None:
            exchanges = exchanges[:self.config.max_exchanges]

        return exchanges

    def _filter_min_transplant_score(self, exchanges: List[str]):
        if not self.config.min_transplant_score:
            return

        delete_indices = []
        for exchange_index, exchange in enumerate(exchanges):
            if exchange.get_min_transplant_score(self._scorer) < self.config.min_transplant_score:
                delete_indices.append(exchange_index)

        for index in reversed(delete_indices):
            del exchanges[index]

    def _filter_required_names(self, exchanges: List[str]):
        if not self.config.required_names:
            return

        delete_indices = []
        for exchange_index, exchange in enumerate(exchanges):
            for name in self.config.required_names:
                if not exchange.contains(name):
                    delete_indices.append(exchange_index)
                    break

        for index in reversed(delete_indices):
            del exchanges[index]

    def _filter_allowed_countries(self, exchanges: List[str]):
        if not self.config.allowed_countries:
            return

        delete_indices = []
        for exchange_index, exchange in enumerate(exchanges):
            for country in exchange.get_countries():
                if country not in self.config.allowed_countries:
                    delete_indices.append(exchange_index)
                    break

        for index in reversed(delete_indices):
            del exchanges[index]

    def _filter_max_distinct_countries_in_chain(self, exchanges: List[str]):
        if self.config.max_countries_in_chain is None:
            return

        delete_indices = []
        for exchange_index, exchange in enumerate(exchanges):
            if exchange.get_max_distinct_countries_in_chain_count() > self.config.max_countries_in_chain:
                delete_indices.append(exchange_index)

        for index in reversed(delete_indices):
            del exchanges[index]

    def _filter_max_number_of_transplants_in_chain(self, exchanges: List[str]):
        if self.config.max_number_of_transplants_in_chain is None:
            return

        delete_indices = []
        for exchange_index, exchange in enumerate(exchanges):
            if exchange.get_max_number_of_transplants() > self.config.max_number_of_transplants_in_chain:
                delete_indices.append(exchange_index)

        for index in reversed(delete_indices):
            del exchanges[index]

    def _save_to_cache(self, exchanges: List[Exchange]):
        serialized_exchanges = [exchange.serialize_to_GET() for exchange in exchanges]

        with open(self.config.cache_path, "w") as cache_file:
            json.dump(serialized_exchanges, cache_file)

    def _load_from_cache(self):
        with open(self.config.cache_path, "r") as cache_file:
            serialized_exchanges = json.load(cache_file)

        patients = self._solver._donors + self._solver._recipients
        exchanges = [Exchange.deserialize_from_GET(serialized_exchange, patients) for serialized_exchange in
                     serialized_exchanges]
        return exchanges

    def _construct_solver(self) -> ExchangeSolverBase:
        solver_constructor_name = self.config.underlying_solver_constructor_name
        solver_constructor = solver_constructor_name_to_solver_constructor[solver_constructor_name]
        solver_config_constructor = solver_constructor_name_to_solver_config_constructor[solver_constructor_name]
        solver_config = solver_config_constructor.deserialize(self.config.underlying_solver_config)
        solver = solver_constructor(self.donors, self.recipients, self.scorer, solver_config)
        return solver
