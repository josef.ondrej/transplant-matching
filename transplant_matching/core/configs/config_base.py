import json
from typing import Dict


class ConfigBase(object):
    def __init__(self):
        pass

    def serialize(self) -> Dict:
        raise NotImplementedError("Has to be overriden.")

    @classmethod
    def deserialize(cls, parameters: Dict) -> "ConfigBase":
        raise NotImplementedError("Has to be overriden.")

    def dump(self, path: str = None):
        serialized = self.serialize()

        with open(path, "w") as out_file:
            json.dump(serialized, out_file)

    @classmethod
    def load(cls, path: str = None) -> "ConfigBase":
        with open(path, "r") as in_file:
            parameters = json.load(in_file)

        config = cls.deserialize(parameters)
        return config
