from typing import Dict, List, Tuple

from transplant_matching.core.configs.scorer_config import ScorerConfig

CRITICAL_ANTIBODY_CONCENTRATIONS = "critical_antibody_concentrations"
ENFORCE_SAME_BLOOD_GROUP = "enforce_same_blood_group"

DEFAULT_PARAMETERS = {
    "critical_antibody_concentrations": {
        "A": {
            "A1": 2000.0,
            "A2": 2000.0,
            "A203": 2000.0,
            "A210": 2000.0,
            "A3": 2000.0,
            "A11": 2000.0,
            "A23": 2000.0,
            "A9": 2000.0,
            "A24": 2000.0,
            "A2403": 2000.0,
            "A25": 2000.0,
            "A10": 2000.0,
            "A29": 2000.0,
            "A19": 2000.0,
            "A30": 2000.0,
            "A31": 2000.0,
            "A32": 2000.0,
            "A33": 2000.0,
            "A34": 2000.0,
            "A35": 2000.0,
            "A36": 2000.0,
            "A43": 2000.0,
            "A66": 2000.0,
            "A68": 2000.0,
            "A28": 2000.0,
            "A74": 2000.0,
            "A80": 2000.0,
        },

        "B": {
            "B7": 2000.0,
            "B703": 2000.0,
            "B8": 2000.0,
            "B13": 2000.0,
            "B18": 2000.0,
            "B27": 2000.0,
            "B2708": 2000.0,
            "B35": 2000.0,
            "B37": 2000.0,
            "B38": 2000.0,
            "B39": 2000.0,
            "B3901": 2000.0,
            "B3902": 2000.0,
            "B4005": 2000.0,
            "B41": 2000.0,
            "B42": 2000.0,
            "B44": 2000.0,
            "B45": 2000.0,
            "B46": 2000.0,
            "B47": 2000.0,
            "B48": 2000.0,
            "B49": 2000.0,
            "B50": 2000.0,
            "B51": 2000.0,
            "B5102": 2000.0,
            "B5103": 2000.0,
            "B52": 2000.0,
            "B53": 2000.0,
            "B54": 2000.0,
            "B55": 2000.0,
            "B56": 2000.0,
            "B57": 2000.0,
            "B58": 2000.0,
            "B59": 2000.0,
            "B60": 2000.0,
            "B61": 2000.0,
            "B62": 2000.0,
            "B63": 2000.0,
            "B64": 2000.0,
            "B65": 2000.0,
            "B67": 2000.0,
            "B71": 2000.0,
            "B72": 2000.0,
            "B73": 2000.0,
            "B75": 2000.0,
            "B76": 2000.0,
            "B77": 2000.0,
            "B78": 2000.0,
            "B81": 2000.0,
            "B82": 2000.0
        },

        "Cw": {
            "Cw1": 2000.0,
            "Cw2": 2000.0,
            "Cw4": 2000.0,
            "Cw5": 2000.0,
            "Cw6": 2000.0,
            "Cw7": 2000.0,
            "Cw8": 2000.0,
            "Cw9": 2000.0,
            "Cw10": 2000.0
        },

        "DR": {
            "DR1": 2000.0,
            "DR2": 2000.0,
            "DR3": 2000.0,
            "DR103": 2000.0,
            "DR4": 2000.0,
            "DR5": 2000.0,
            "DR6": 2000.0,
            "DR7": 2000.0,
            "DR8": 2000.0,
            "DR9": 2000.0,
            "DR10": 2000.0,
            "DR11": 2000.0,
            "DR12": 2000.0,
            "DR13": 2000.0,
            "DR14": 2000.0,
            "DR1403": 2000.0,
            "DR1404": 2000.0,
            "DR15": 2000.0,
            "DR16": 2000.0,
            "DR17": 2000.0,
            "DR18": 2000.0,
            "DR51": 2000.0,
            "DR52": 2000.0,
            "DR53": 2000.0
        },

        "DQ": {
            "DQ1": 2000.0,
            "DQ2": 2000.0,
            "DQ3": 2000.0,
            "DQ4": 2000.0,
            "DQ5": 2000.0,
            "DQ6": 2000.0,
            "DQ7": 2000.0,
            "DQ8": 2000.0,
            "DQ9": 2000.0
        }
    },
    "enforce_same_blood_group": True
}


class HLAScorerConfig(ScorerConfig):
    def __init__(self):
        self._parameters = DEFAULT_PARAMETERS

    def serialize(self) -> Dict:
        return self._parameters

    @classmethod
    def deserialize(cls, parameters: Dict) -> "HLAScorerConfig":
        cls._validate(parameters)
        config = HLAScorerConfig()
        config._parameters = parameters
        return config

    @property
    def critical_antibody_concentrations(self) -> Dict[str, float]:
        antibody_concentrations_flattened = {antibody: concentration for hla_group_name, concentrations in
                                             self.hla_system_dictionary.items()
                                             for antibody, concentration in concentrations.items()}
        return antibody_concentrations_flattened

    @property
    def hla_system_dictionary(self):
        return self._parameters[CRITICAL_ANTIBODY_CONCENTRATIONS]

    @property
    def enforce_same_blood_group(self) -> bool:
        return (bool)(self._parameters[ENFORCE_SAME_BLOOD_GROUP])

    @property
    def hla_system_locuses(self) -> List[str]:
        return list(self.hla_system_dictionary.keys())

    def get_critical_antibody_concentration(self, antigen: str) -> float:
        concentration = self.critical_antibody_concentrations.get(antigen, 0)
        return concentration

    def get_critical_antibody_concentrations_for_group(self, group: str) -> List[Tuple[str, float]]:
        concentration_dict = self._parameters[CRITICAL_ANTIBODY_CONCENTRATIONS][group]
        concentrations = [(k, v) for k, v in concentration_dict.items()]
        concentrations.sort(key=lambda item: (int)(item[0][len(group):]))
        return concentrations

    def set_enforce_same_blood_group(self, enforce_same_blood_group: bool):
        self._parameters[ENFORCE_SAME_BLOOD_GROUP] = enforce_same_blood_group

    def set_critical_antibody_concentration(self, antibody_name: str, concentration: float):
        for hla_group_name, concentrations in self.hla_system_dictionary.items():
            if antibody_name.startswith(hla_group_name):
                concentrations[antibody_name] = concentration

    @classmethod
    def _validate(cls, parameters: Dict):
        valid = CRITICAL_ANTIBODY_CONCENTRATIONS in parameters
        valid = valid and (ENFORCE_SAME_BLOOD_GROUP in parameters)
        if not valid:
            raise AssertionError("Parameters not valid. ")

    def update_from_string_dict(self, arguments: Dict[str, str]):
        enforce_same_blood_group_hidden = arguments.get("enforce_same_blood_group_hidden", "off")
        if enforce_same_blood_group_hidden == "on":
            enforce_same_blood_group = arguments.get("enforce_same_blood_group", "off")
            enforce_same_blood_group = enforce_same_blood_group == "on"
            self.set_enforce_same_blood_group(enforce_same_blood_group)

        antibody_names = list(self.critical_antibody_concentrations.keys())

        for antibody_name in antibody_names:
            antibody_critical_concentration = arguments.get(antibody_name, None)
            if antibody_critical_concentration is not None:
                try:
                    antibody_critical_concentration = (float)(antibody_critical_concentration)
                    self.set_critical_antibody_concentration(antibody_name, antibody_critical_concentration)
                except:
                    print(f"[ERROR] Parsing {antibody_name} antibody concentration: {antibody_critical_concentration}")
