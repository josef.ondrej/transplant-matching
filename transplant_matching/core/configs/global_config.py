from typing import Dict

from transplant_matching.core.configs.scorer_config import ScorerConfig
from transplant_matching.core.configs.solver_config import SolverConfig

scorer_config_constructor_name_to_constructor = {"ScorerConfig": ScorerConfig}
solver_config_constructor_name_to_constructor = {"SolverConfig": SolverConfig}

FIELD_solver_config = "solver_config"
FILED_scorer_config = "scorer_config"
FIELD_solver_config_constructor_name = "solver_config_constructor_name"
FIELD_scorer_config_constructor_name = "scorer_config_constructor_name"


class GlobalConfig(object):
    def __init__(self, scorer_config: ScorerConfig, solver_config: SolverConfig):
        self._scorer_config = scorer_config
        self._solver_config = solver_config

    def serialize(self) -> Dict:
        serialized = {FIELD_solver_config: self._solver_config.serialize(),
                      FILED_scorer_config: self._scorer_config_serialize(),
                      FIELD_solver_config_constructor_name: self._solver_config.__class__.__name__,
                      FIELD_scorer_config_constructor_name: self._scorer_config.__class__.__name__}

        return serialized

    @classmethod
    def deserialize(cls, parameters: Dict) -> "GlobalConfig":
        solver_config_constructor_name = parameters[FIELD_solver_config_constructor_name]
        scorer_config_constructor_name = parameters[FIELD_scorer_config_constructor_name]

        solver_config = parameters[FIELD_solver_config]
        scorer_config = parameters[FILED_scorer_config]

        solver_config_constructor = solver_config_constructor_name_to_constructor[solver_config_constructor_name]
        scorer_config_constructor = scorer_config_constructor_name_to_constructor[scorer_config_constructor_name]

        solver_config = solver_config_constructor.deserialize(solver_config)
        scorer_config = scorer_config_constructor.deserialize(scorer_config)

        return GlobalConfig(scorer_config, solver_config)
