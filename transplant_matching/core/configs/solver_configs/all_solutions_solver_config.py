from typing import Dict

from transplant_matching.core.configs.solver_config import SolverConfig

FIELD_max_transplants_in_chain = "max_transplants_in_chain"


class AllSolutionsSolverConfig(SolverConfig):
    def __init__(self, max_transplants_in_chain: int = None):
        self._max_transplants_in_chain = max_transplants_in_chain

    @property
    def max_transplants_in_chain(self) -> int:
        return self._max_transplants_in_chain

    def serialize(self) -> Dict:
        parameters = {FIELD_max_transplants_in_chain: self._max_transplants_in_chain}
        return parameters

    @classmethod
    def deserialize(cls, parameters: Dict) -> "AllSolutionsSolverConfig":
        max_transplants_in_chain = parameters[FIELD_max_transplants_in_chain]
        return AllSolutionsSolverConfig(max_transplants_in_chain=max_transplants_in_chain)
