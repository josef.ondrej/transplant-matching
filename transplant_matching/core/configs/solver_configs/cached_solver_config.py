import typing
from typing import List, Dict

from transplant_matching.core.configs.solver_config import SolverConfig

FIELD_underlying_solver_constructor_name = "underlying_solver_constructor_name"
FIELD_underlying_solver_config = "underlying_solver_config"
FIELD_cache_path = "cache_path"
FIELD_cache_valid = "cache_valid"
FIELD_required_names = "required_names"
FIELD_allowed_countries = "allowed_countries"
FIELD_max_countries_in_chain = "max_countries_in_chain"
FIELD_max_number_of_transplants_in_chain = "max_number_of_transplants_in_chain"
FIELD_min_transplant_score = "min_transplant_score"
FIELD_max_exchanges = "max_exchanges"


class CachedSolverConfig(SolverConfig):
    def __init__(self,
                 underlying_solver_constructor_name: str,
                 underlying_solver_config: Dict,
                 cache_path: str,
                 cache_valid: bool = True,
                 required_names: List[str] = None,
                 allowed_countries: List[str] = None,
                 max_countries_in_chain: int = None,
                 max_number_of_transplants_in_chain: int = None,
                 min_transplant_score: float = None,
                 max_exchanges: int = 100):
        self._parameters = {
            FIELD_underlying_solver_constructor_name: underlying_solver_constructor_name,
            FIELD_underlying_solver_config: underlying_solver_config,
            FIELD_cache_path: cache_path,
            FIELD_cache_valid: cache_valid,
            FIELD_required_names: required_names or [],
            FIELD_allowed_countries: allowed_countries or [],
            FIELD_max_countries_in_chain: max_countries_in_chain,
            FIELD_max_number_of_transplants_in_chain: max_number_of_transplants_in_chain,
            FIELD_min_transplant_score: min_transplant_score,
            FIELD_max_exchanges: max_exchanges,
        }

    @property
    def underlying_solver_constructor_name(self) -> str:
        return self._parameters[FIELD_underlying_solver_constructor_name]

    @property
    def underlying_solver_config(self) -> Dict:
        return self._parameters[FIELD_underlying_solver_config]

    @property
    def cache_path(self) -> str:
        return self._parameters[FIELD_cache_path]

    @property
    def cache_valid(self) -> bool:
        return self._parameters[FIELD_cache_valid]

    @property
    def required_names(self) -> List[str]:
        return self._parameters[FIELD_required_names]

    @property
    def allowed_countries(self) -> List[str]:
        return self._parameters[FIELD_allowed_countries]

    @property
    def max_countries_in_chain(self) -> int:
        return self._parameters[FIELD_max_countries_in_chain]

    @property
    def max_number_of_transplants_in_chain(self) -> int:
        return self._parameters[FIELD_max_number_of_transplants_in_chain]

    @property
    def min_transplant_score(self) -> float:
        return self._parameters[FIELD_min_transplant_score]

    @property
    def max_exchanges(self) -> int:
        return self._parameters[FIELD_max_exchanges]

    def serialize(self) -> Dict:
        return self._parameters

    @classmethod
    def deserialize(cls, parameters: Dict) -> "CachedSolverConfig":
        underlying_solver_constructor_name = parameters[FIELD_underlying_solver_constructor_name]
        underlying_solver_config = parameters[FIELD_underlying_solver_config]
        cache_path = parameters[FIELD_cache_path]
        cache_valid = parameters[FIELD_cache_valid]
        required_names = parameters[FIELD_required_names]
        allowed_countries = parameters[FIELD_allowed_countries]
        max_countries_in_chain = parameters[FIELD_max_countries_in_chain]
        max_number_of_transplants_in_chain = parameters[FIELD_max_number_of_transplants_in_chain]
        min_transplant_score = parameters[FIELD_min_transplant_score]
        max_exchanges = parameters[FIELD_max_exchanges]

        config = CachedSolverConfig(underlying_solver_constructor_name=underlying_solver_constructor_name,
                                    underlying_solver_config=underlying_solver_config,
                                    cache_path=cache_path,
                                    cache_valid=cache_valid,
                                    required_names=required_names,
                                    allowed_countries=allowed_countries,
                                    max_countries_in_chain=max_countries_in_chain,
                                    max_number_of_transplants_in_chain=max_number_of_transplants_in_chain,
                                    min_transplant_score=min_transplant_score,
                                    max_exchanges=max_exchanges)
        return config

    def validate_cache(self):
        self._parameters[FIELD_cache_valid] = True

    def invalidate_cache(self):
        self._parameters[FIELD_cache_valid] = False

    def update_parameter_value(self, parameter_name: str, parameter_value: str):
        if parameter_name not in self._parameters:
            raise ValueError(f"[ERROR] Parameter {parameter_name} not valid for CachedSolverConfig.")

        self._parameters[parameter_name] = parameter_value

    def update_from_string_dict(self, arguments: Dict[str, str], list_separator: str = ";", verbose: bool = False):
        for arg_name, arg_type in CachedSolverConfig.__init__.__annotations__.items():

            pass_argument = False
            arg_in_arguments = False

            try:
                arg_in_arguments = arg_name in arguments
                raw_arg_value = arguments.get(arg_name, None)

                if arg_type in [float, str, int]:
                    if raw_arg_value.strip() == "":
                        arg_value = None
                    else:
                        arg_value = arg_type(raw_arg_value)
                elif isinstance(arg_type, typing._GenericAlias) and arg_type._name == "List":
                    if raw_arg_value.strip() == "":
                        arg_value = []
                    else:
                        inner_arg_type = arg_type.__args__[0]
                        arg_value = [inner_arg_type(av) for av in raw_arg_value.split(list_separator)]
                else:
                    pass_argument = True

            except Exception as e:
                pass_argument = True

                if verbose:
                    print(f"[ERROR] Parsing argument {arg_name}")

            if arg_in_arguments and not pass_argument:
                self._parameters[arg_name] = arg_value


if __name__ == "__main__":
    print(CachedSolverConfig.__init__.__annotations__)
