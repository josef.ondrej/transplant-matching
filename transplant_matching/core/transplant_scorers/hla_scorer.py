import os

import numpy as np

from transplant_matching.core.configs.scorer_configs.hla_scorer_config import HLAScorerConfig
from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.transplant_scorers.compatibility_index import get_compatibility_index
from transplant_matching.core.transplant_scorers.transplant_scorer_base import TransplantScorerBase, UNACCEPTABLE_SCORE
from transplant_matching.database.tinydb_database import TinyDBDatabase


class HLAScorer(TransplantScorerBase):
    def __init__(self, parameters: HLAScorerConfig = None):
        super().__init__()
        self._parameters = parameters or HLAScorerConfig()

    def score_transplant(self, donor: Donor, recipient):
        if not donor.blood_group in recipient.acceptable_blood_groups:
            return UNACCEPTABLE_SCORE

        if self._parameters.enforce_same_blood_group:
            if donor.blood_group != recipient.blood_group:
                return UNACCEPTABLE_SCORE

        for antigen in donor.hla_antigens:
            if antigen in recipient.hla_antibodies:
                recipient_antibody_concentration = recipient.hla_antibodies[antigen]
                if recipient_antibody_concentration is None:
                    recipient_antibody_concentration = np.inf

                critical_antibody_concentration = self._parameters.get_critical_antibody_concentration(antigen)

                if recipient_antibody_concentration > critical_antibody_concentration:
                    return UNACCEPTABLE_SCORE

        compatibility = get_compatibility_index(donor.hla_antigens, recipient.hla_antigens)
        return compatibility


if __name__ == "__main__":
    this_file_path = os.path.realpath(__file__)
    package_path = "/".join(this_file_path.split("/")[:-2])
    resources_path = package_path + "/resources/"
    DB_PATH = resources_path + "db.json"
    database = TinyDBDatabase(DB_PATH)

    donors = database.get_donors()
    recipients = database.get_recipients()

    donor_name_to_donor = {donor.name: donor for donor in donors}

    scorer = HLAScorer()

    recipient_to_suitable_donors = dict()
    for recipient in recipients:
        original_donor = donor_name_to_donor[recipient.donor_name]
        suitable_donors = []
        for donor in donors:
            transplant_score = scorer.score_transplant_considering_original_donor(donor, original_donor, recipient)
            if transplant_score > 0:
                suitable_donors.append(donor.name)

        recipient_to_suitable_donors[recipient.name] = suitable_donors

    for recipient, suitable_donors in recipient_to_suitable_donors.items():
        if len(suitable_donors) > 0:
            suitable_donors_str = ", ".join(suitable_donors)
            print(f"Recipient: {recipient}")
            print(f"Suitable donors: {suitable_donors_str}")
            print("-" * 100)

    recipients_with_some_suitable_donors = [recipient for recipient, donors in recipient_to_suitable_donors.items() if
                                            len(donors) > 0]

    print(f"\n\nThere are {len(recipients_with_some_suitable_donors)} recipients with at least one suitable donor. \n")
