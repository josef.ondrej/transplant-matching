from typing import List

from transplant_matching.core.transplant_scorers.hla_system import get_simplified_classification

compatibility_index = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
A_matches_count = [0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2]
B_matches_count = [0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1, 2, 2, 2]
DR_matches_count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2]

ABDR_matches_count_to_compatibility_index = {
    f"{A}{B}{DR}": CI for A, B, DR, CI in
    zip(A_matches_count, B_matches_count, DR_matches_count, compatibility_index)
}


def _get_locus_antigens(hla_antigens: List[str], locus: str):
    return [antigen for antigen in hla_antigens if antigen.startswith(locus)]


def _boost_to_two(antigens: List[str]) -> List[str]:
    antigens = list(set(antigens))
    if len(antigens) == 1:
        antigen = antigens[0]
        antigens = [antigen for i in range(2)]

    return antigens


def _get_matches_count(hla_antigens_1: List[str], hla_antigens_2: List[str], locus: str):
    locus_antigens = [_get_locus_antigens(antigens, locus)
                      for antigens in [hla_antigens_1, hla_antigens_2]]

    simplified_locus_antigens = [[get_simplified_classification(antigen) for antigen in antigens]
                                 for antigens in locus_antigens]

    simplified_antigen_pairs = [_boost_to_two(antigens) for antigens in simplified_locus_antigens]
    antigen_pair_1, antigen_pair_2 = simplified_antigen_pairs

    matches_count = 0

    for antigen in antigen_pair_1:
        if antigen in antigen_pair_2:
            matches_count += 1
            antigen_pair_2.pop(antigen_pair_2.index(antigen))

    return matches_count


def get_compatibility_index(hla_antigens_1: List[str], hla_antigens_2: List[str]) -> float:
    A_matches_count = _get_matches_count(hla_antigens_1, hla_antigens_2, "A")
    B_matches_count = _get_matches_count(hla_antigens_1, hla_antigens_2, "B")
    DR_matches_count = _get_matches_count(hla_antigens_1, hla_antigens_2, "DR")
    key = f"{A_matches_count}{B_matches_count}{DR_matches_count}"
    compatibility_index = ABDR_matches_count_to_compatibility_index.get(key)
    return (float)(compatibility_index)
