simplified_to_extended = {
    "A9": ["A23", "A24"],
    "A10": ["A25", "A26", "A34", "A66"],
    "A19": ["A29", "A30", "A31", "A32", "A33", "A74"],
    "A28": ["A68", "A69"],

    "B16": ["B38", "B39"],
    "B12": ["B44", "B45"],
    "B21": ["B49", "B50"],
    "B5": ["B51", "B52"],
    "B22": ["B54", "B55", "B56"],
    "B17": ["B57", "B58"],
    "B40": ["B60", "B61"],
    "B15": ["B62", "B63", "B75", "B76", "B77"],
    "B14": ["B64", "B65"],
    "B70": ["B71", "B72"],

    "DR5": ["DR11", "DR12"],
    "DR6": ["DR13", "DR14"],
    "DR2": ["DR15", "DR16"],
    "DR3": ["DR17", "DR18"],

    "DQ1": ["DQ5", "DQ6"],
    "DQ3": ["DQ7", "DQ8", "DQ9", "DQ2", "DQ4"]
}


def get_simplified_classification(extended_antigen_classification: str) -> str:
    for simplified_value, extended_values in simplified_to_extended.items():
        if extended_antigen_classification in extended_values:
            return simplified_value

    return extended_antigen_classification
