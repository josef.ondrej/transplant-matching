from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient
import numpy as np

UNACCEPTABLE_SCORE = np.NINF
DEFAULT_DONOR_RECIPIENT_PAIR_SCORE = np.NAN


def replace_special_scores(score_matrix: np.ndarray, unacceptable_score: np.float, pair_score: np.float) -> np.array:
    score_matrix[score_matrix == UNACCEPTABLE_SCORE] = unacceptable_score
    score_matrix[np.isnan(score_matrix)] = pair_score
    return score_matrix


class TransplantScorerBase(object):
    def __init__(self):
        pass

    def score_transplant(self, donor: Donor, recipient: Recipient) -> float:
        """
        Score of a single transplant using just information about the donor and recipient.
        """
        raise NotImplementedError("Has to be overriden")

    def score_transplant_considering_original_donor(self, donor: Donor, recipients_donor: Donor,
                                                    recipient: Recipient) -> float:
        """
        Score of a single transplant using information about donor and recipient and considering also the match
        with the original recipients_donor. If the donor has worse or same score as the recipients_donor then
        the exchange is scored as unacceptable.
        """
        original_score = self.score_transplant(recipients_donor, recipient)
        new_score = self.score_transplant(donor, recipient)

        if donor.name == recipients_donor.name:
            return DEFAULT_DONOR_RECIPIENT_PAIR_SCORE

        if new_score > original_score:
            return new_score
        else:
            return UNACCEPTABLE_SCORE
