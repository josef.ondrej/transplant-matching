import re

PATIENT_NAME_PREFIX_TO_STATE = {"P": "CZE",
                                "G": "AUT",
                                "I": "IL",
                                "W": "AUT",
                                "IS": "AUT"}


def get_pair_name(donor_or_recipient_name: str) -> str:
    matches = re.findall("[A-Z]+-[0-9]+", donor_or_recipient_name)
    return matches[0]


def are_pair(donor_name: str, recipient_name: str) -> bool:
    return get_pair_name(donor_name) == get_pair_name(recipient_name)


def get_country(patient_name: str) -> str:
    prefix = patient_name.split("-")[0]
    return PATIENT_NAME_PREFIX_TO_STATE[prefix]
