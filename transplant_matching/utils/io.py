import re
import webbrowser
from tempfile import NamedTemporaryFile
from typing import List, Tuple, Any, Optional

import pandas as pd
from pandas import read_csv

from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.recipient import Recipient

DONOR_NAME = "donor"
BLOOD_GROUP_DONOR = "blood group donor"
TYPIZATION_DONOR = "typization donor"
RECIPIENT_NAME = "recipient"
BLOOD_GROUP_RECIPIENT = "blood group recipient"
TYPIZATION_RECIPIENT = "typization recipient"
LUMINEX_1 = "luminex varianta 1"
LUMINEX_2 = "luminex  cut-off (2000 mfi) varianta 2"
ACCEPTABLE_BLOOD_GROUP = "acceptable blood group"


def read_raw_data(file_path: str) -> pd.DataFrame:
    dataframe = read_csv(file_path, sep=";")
    return dataframe


def browse_dataframe(dataframe: pd.DataFrame):
    with NamedTemporaryFile(delete=False, suffix='.html') as f:
        f.write(bytes(dataframe.to_html(), "utf-8"))
    webbrowser.open(f.name)


def get_markers(markers: str) -> Optional[List[str]]:
    if not is_available(markers):
        return None
    antigens = re.split("[,; ()]+", str(markers))
    return [antigen for antigen in antigens if len(antigen) > 0]


def is_valid_blood_group(blood_group: str) -> bool:
    return blood_group.upper() in ["0", "A", "B", "AB"]


def get_blood_group(blood_group_string: Any) -> Optional[str]:
    blood_group = str(blood_group_string).strip().upper()
    if not is_available(blood_group):
        return None
    assert is_valid_blood_group(blood_group)
    return blood_group


def get_blood_groups(blood_groups_string: str) -> Optional[List[str]]:
    if not is_available(str(blood_groups_string)):
        return None
    blood_groups = re.split("[,; ()]+", str(blood_groups_string))
    return [blood_group.upper() for blood_group in blood_groups if is_valid_blood_group(blood_group)]


def dataframe_to_donor_recipients(dataframe: pd.DataFrame) -> Tuple[List[Donor], List[Recipient]]:
    donors = []
    recipients = []
    dataframe.columns = [name.strip().lower() for name in dataframe.columns]

    for index, row in dataframe.iterrows():
        donor_name = str(row[DONOR_NAME])
        donor_blood_group = get_blood_group(row[BLOOD_GROUP_DONOR])
        donor_antigens = get_markers(row[TYPIZATION_DONOR])

        donor = Donor(donor_name, donor_blood_group, donor_antigens)
        donors.append(donor)

        recipient_name = str(row[RECIPIENT_NAME])
        if is_available(recipient_name):
            recipient_blood_group = get_blood_group(row[BLOOD_GROUP_RECIPIENT])
            recipient_hla_antigens = get_markers(row[TYPIZATION_RECIPIENT])
            recipient_hla_antibodies = {marker: None for marker in get_markers(row[LUMINEX_1]) or []}
            recipient_acceptable_blood_group = get_blood_groups(row[ACCEPTABLE_BLOOD_GROUP])
            recipient = Recipient(recipient_name, recipient_blood_group, recipient_hla_antigens,
                                  recipient_hla_antibodies, recipient_acceptable_blood_group, donor_name)
            recipients.append(recipient)

    return (donors, recipients)


def is_available(name: str) -> bool:
    stripped_name = name.strip().lower()
    return stripped_name not in ["negativní", "nan"]


if __name__ == "__main__":
    file_path = "../resources/PV28.csv"
    dataframe = read_raw_data(file_path)
    # browse_dataframe(dataframe)
    print(dataframe)

    dataframe_to_donor_recipients(dataframe)
