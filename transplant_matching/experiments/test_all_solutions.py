from transplant_matching.core.exchange_solvers.all_solutions_solver import AllSolutionsSolver
from transplant_matching.core.exchange_solvers.cached_solver import CachedSolver
from transplant_matching.core.transplant_scorers.hla_scorer import HLAScorer, ENFORCE_SAME_BLOOD_GROUP
from transplant_matching.database.tinydb_database import TinyDBDatabase

database_path = "transplant_matching/resources/db.json"

database = TinyDBDatabase(database_path)
donors, recipients = database.get_donors(), database.get_recipients()
scorer = HLAScorer()
scorer._parameters._parameters[ENFORCE_SAME_BLOOD_GROUP] = False

solver = CachedSolver(AllSolutionsSolver(donors, recipients, scorer, 4, verbose=True),
                      cache_path="/tmp/cache.json",
                      required_names=None,
                      allowed_countries=None,
                      max_countries_in_chain=2)

exchanges = solver.get_exchange_iterator()
print(len(exchanges))