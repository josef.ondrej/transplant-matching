from typing import List, Tuple

import numpy as np
import scipy.optimize as optimize

from transplant_matching.core.exchange_solvers.hungarian_algorithm_solver import HungarianAlgorithmSolver

score_matrix = np.loadtxt("./transplant_matching/resources/score_matrix.csv", delimiter=",")
# database_path = "transplant_matching/resources/db.json"
# score_matrix = get_score_matrix(database_path)
# score_matrix = [[1, 2, 3, 4],
#                 [2, 3, 4, 5],
#                 [4, 5, 6, 2],
#                 [3, 3, 3, 1],
#                 [2, 2, 3, 1]]
#
# score_matrix = np.array(score_matrix)

cost_matrix = np.max(score_matrix) - score_matrix

solver = HungarianAlgorithmSolver(None, None, None)

assignment_iterator = solver._solve(score_matrix)

best_assignment = optimize.linear_sum_assignment(cost_matrix)
best_assignment = list(zip(*best_assignment))


def get_assignment_score(assignment: List[Tuple[int, int]], score_matrix: np.ndarray):
    return sum([score_matrix[i, j] for i, j in assignment if score_matrix[i, j] != -1])


best_assignment_score = get_assignment_score(best_assignment, score_matrix)
print(f"Best assignment score: {best_assignment_score}")
print([get_assignment_score(assignment, score_matrix) for assignment in assignment_iterator])
