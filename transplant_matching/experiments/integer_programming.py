import numpy as np

from transplant_matching.core.exchange_solvers.integer_programming_solver import IntegerProgrammingSolver
from transplant_matching.core.transplant_scorers.hla_scorer import HLAScorer
from transplant_matching.database.tinydb_database import TinyDBDatabase

if __name__ == "__main__":
    database_path = "./transplant_matching/resources/db.json"
    database = TinyDBDatabase(database_path)
    donors, recipients = database.get_donors(), database.get_recipients()

    scorer = HLAScorer()
    solver = IntegerProgrammingSolver(donors, recipients, scorer, 4, True)
    score_matrix = solver._create_score_matrix()
    assignment = next(solver._solve(score_matrix))

    print(assignment)
