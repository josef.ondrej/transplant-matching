import matplotlib.pyplot as plt
import numpy as np

from transplant_matching.core.exchange_solvers.exchange_solver_base import ExchangeSolverBase
from transplant_matching.core.transplant_scorers.hla_scorer import HLAScorer
from transplant_matching.database.tinydb_database import TinyDBDatabase


def get_score_matrix(database_path: str) -> np.ndarray:
    database = TinyDBDatabase(database_path)
    donors, recipients = database.get_donors(), database.get_recipients()
    scorer = HLAScorer()
    solver = ExchangeSolverBase(donors, recipients, scorer)
    score_matrix = solver._create_score_matrix()
    return score_matrix


if __name__ == "__main__":
    database_path = "transplant_matching/resources/db.json"
    score_matrix_file_path = "./transplant_matching/resources/score_matrix.csv"

    score_matrix = get_score_matrix(database_path)

    plt.imshow(score_matrix)
    plt.show()
    np.savetxt(score_matrix_file_path, score_matrix, delimiter=",")
