from typing import List

import numpy as np
from graph_tool import Graph, topology
from graph_tool.draw import graph_draw


def graph_from_adjacency_matrix(adjacency_matrix: List[List[int]]):
    graph = Graph()
    ix_to_vertex = {i: graph.add_vertex() for i in range(len(adjacency_matrix))}

    for i, j in zip(*np.where(adjacency_matrix)):
        vi, vj = (ix_to_vertex[k] for k in (i, j))
        graph.add_edge(vi, vj)

    return graph


if __name__ == "__main__":
    adjacency_matrix = [
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ]

    graph = graph_from_adjacency_matrix(adjacency_matrix)

    graph_draw(graph, vertex_text=graph.vertex_index, vertex_font_size=18,
               output_size=(600, 600), output="graph.png")

    max_cliques = topology.max_cliques(graph)
    for clique in max_cliques:
        print(clique)
