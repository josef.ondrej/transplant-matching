from typing import Dict, List, Optional

from tinydb import Query, TinyDB

from transplant_matching.database.database_base import DatabaseBase
from transplant_matching.core.patients.patient import ID


class TinyDBDatabase(DatabaseBase):
    def __init__(self, db_file_path: str = "./resources/db.json"):
        super().__init__()
        self._db_file_path = db_file_path
        self._db = TinyDB(self._db_file_path)

    def _insert_record(self, record: Dict):
        if self._get_record(record[ID]) is None:
            self._db.insert(record)

    def _get_record(self, id: str) -> Optional[Dict]:
        query = Query()
        search_result = self._db.search(query[ID] == id)
        if len(search_result) == 0:
            return None
        else:
            return search_result[0]

    def _list_patient_ids(self) -> List[str]:
        query = Query()
        search_result = self._db.search(query._id)
        search_result = [] or search_result
        return [record[ID] for record in search_result]

    def purge(self):
        self._db.purge_tables()
