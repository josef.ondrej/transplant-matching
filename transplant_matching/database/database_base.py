from typing import Dict, List, Optional

from transplant_matching.core.patients.donor import Donor
from transplant_matching.core.patients.patient import Patient, PAYLOAD, TYPE
from transplant_matching.core.patients.recipient import Recipient
from transplant_matching.utils.io import read_raw_data, dataframe_to_donor_recipients

PATIENT_TYPE_TO_CONSTRUCTOR = {"Donor": Donor,
                               "Recipient": Recipient}


class DatabaseBase(object):
    def __init__(self):
        pass

    def _insert_record(self, record: Dict):
        raise NotImplementedError("Has to be overriden.")

    def _get_record(self, id: str) -> Dict:
        raise NotImplementedError("Has to be overriden.")

    def _list_patient_ids(self):
        raise NotImplementedError("Has to be overriden.")

    def insert_patient(self, patient: Patient):
        patient_record = patient.serialize_to_dict()
        self._insert_record(patient_record)

    def get_patient(self, patient_id: str) -> Optional[Patient]:
        patient_record = self._get_record(patient_id)
        if patient_record is None:
            return None

        patient_data = patient_record[PAYLOAD]
        patient_type = patient_record[TYPE]
        patient_constructor = PATIENT_TYPE_TO_CONSTRUCTOR[patient_type]
        patient = patient_constructor(**patient_data)
        return patient

    def get_patients(self) -> List[Patient]:
        patient_ids = self._list_patient_ids()
        all_patients = [self.get_patient(patient_id) for patient_id in patient_ids]
        return all_patients

    def get_donors(self) -> List[Donor]:
        all_patients = self.get_patients()
        return [patient for patient in all_patients if isinstance(patient, Donor)]

    def get_recipients(self) -> List[Recipient]:
        all_patients = self.get_patients()
        return [patient for patient in all_patients if isinstance(patient, Recipient)]

    def load_from_excel(self, excel_file: bytes) -> int:
        dataframe = read_raw_data(excel_file)
        donors, recipients = dataframe_to_donor_recipients(dataframe)

        for patient in donors + recipients:
            self.insert_patient(patient)

        return len(donors) + len(recipients)
