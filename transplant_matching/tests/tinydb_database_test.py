from tinydb import Query

from transplant_matching.database.tinydb_database import TinyDBDatabase
from transplant_matching.core.patients.donor import Donor

DB_PATH = "./transplant_matching/resources/db.json"
database = TinyDBDatabase(DB_PATH)

patient_id = "XYZ-TEST"
donor = Donor(patient_id, "A", ["A1", "A2"])
for i in range(10):
    database.insert_patient(donor)

query = Query()
search_result = database._db.search(query._id == patient_id)
assert len(search_result) == 1
