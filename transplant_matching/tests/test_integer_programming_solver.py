from pprint import pprint

from transplant_matching.core.exchange_solvers.integer_programming_solver import IntegerProgrammingSolver

if __name__ == "__main__":
    solver = IntegerProgrammingSolver([], [], None)

    potential_recipients = {1: [20, 30, 40], 2: [30, 40, 50], 3: [40, 50, 60], 4: [50, 60, 70], 5: [60, 70, 80],
                            6: [70, 80, 90], 7: [80, 90, 10], 8: [90, 10, 20], 9: [10, 20, 30], 10: [20, 40, 50],
                            11: [50, 60, 90]}
    recipient_to_original_donor = {10: 1, 20: 2, 30: 3, 40: 4, 50: 5, 60: 6, 70: 7, 80: 8, 90: 9}
    starting_donors = [4, 5, 6]
    starting_donors = [10, 11]

    paths = solver._generate_paths_of_length(3, starting_donors, potential_recipients, recipient_to_original_donor)
    pprint(paths)
