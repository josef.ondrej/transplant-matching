import os
import traceback

from flask import Flask, render_template, request

from transplant_matching.core.configs.scorer_configs.hla_scorer_config import HLAScorerConfig
from transplant_matching.core.configs.solver_configs.all_solutions_solver_config import AllSolutionsSolverConfig
from transplant_matching.core.configs.solver_configs.cached_solver_config import CachedSolverConfig
from transplant_matching.core.exchange_solvers.all_solutions_solver import AllSolutionsSolver
from transplant_matching.core.exchange_solvers.cached_solver import CachedSolver
from transplant_matching.core.transplant_scorers.hla_scorer import HLAScorer
from transplant_matching.database.tinydb_database import TinyDBDatabase
from transplant_matching.web import web_utils

app = Flask(__name__)

# Define general file paths
this_file_path = os.path.realpath(__file__)
package_path = "/".join(this_file_path.split("/")[:-2])
resources_path = package_path + "/resources/"

app.config["UPLOAD_FOLDER"] = resources_path

# Define database and config paths
PATIENT_DB_PATH = resources_path + "db.json"
EXCHANGES_CACHE_PATH = "/tmp/exchange_cache.json"
CACHED_SOLVER_CONFIG_PATH = "/tmp/cached_solver_config.json"
HLA_SCORER_CONFIG_PATH = "/tmp/hla_scorer_config.json"

# Initialize database and configs
database = TinyDBDatabase(PATIENT_DB_PATH)

all_solutions_config = AllSolutionsSolverConfig()
cached_solver_config = CachedSolverConfig(AllSolutionsSolver.__name__,
                                          all_solutions_config.serialize(),
                                          EXCHANGES_CACHE_PATH)
cached_solver_config.dump(CACHED_SOLVER_CONFIG_PATH)

hla_scorer_config = HLAScorerConfig()
hla_scorer_config.dump(HLA_SCORER_CONFIG_PATH)


@app.route("/")
@app.route("/load_patients_from_file", methods=["GET", "POST"])
def load_patients_from_file():
    data_load_status = None
    exception_text = None
    patients_loaded_count = None

    if request.method == "POST":
        try:
            file = request.files["file"]
            database.purge()
            patients_loaded_count = database.load_from_excel(file.stream)
            data_load_status = "SUCCESS"

        except Exception as e:
            traceback.print_exc()
            data_load_status = "FAILURE"
            exception_text = str(e)

    return render_template("load_patients_from_file.html",
                           data_load_status=data_load_status,
                           exception_text=exception_text,
                           patients_loaded_count=patients_loaded_count)


@app.route("/browse_donors")
def browse_donors():
    donors = database.get_donors()
    return render_template("browse_patients.html",
                           patients=donors)


@app.route("/browse_recipients")
def browse_recipients():
    recipients = database.get_recipients()
    return render_template("browse_patients.html",
                           patients=recipients)


@app.route("/scoring", methods=["GET"])
def scoring():
    if request.method == "GET":
        hla_scorer_config.update_from_string_dict(request.args)
        hla_scorer_config.dump(HLA_SCORER_CONFIG_PATH)

        cached_solver_config.invalidate_cache()
        cached_solver_config.dump(CACHED_SOLVER_CONFIG_PATH)

    return render_template("scoring.html", hla_parameters=hla_scorer_config)


@app.route("/browse_exchanges", methods=["GET"])
def browse_exchanges():
    scorer = HLAScorer(hla_scorer_config)

    donors = database.get_donors()
    recipients = database.get_recipients()

    if request.method == "GET":
        cached_solver_config.update_from_string_dict(request.args)
        cached_solver_config.dump(CACHED_SOLVER_CONFIG_PATH)

    solver = CachedSolver(donors, recipients, scorer, cached_solver_config)

    exchange_iterator = solver.get_exchange_iterator()

    selected_exchange_index = request.args.get("selected_exchange_index", 1)

    return render_template("browse_exchanges.html",
                           exchanges=exchange_iterator,
                           scorer=scorer,
                           selected_exchange_index=selected_exchange_index,
                           ui_utils=web_utils,
                           solver_config=cached_solver_config)


if __name__ == "__main__":
    app.run(port=5000, debug=True)
